<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage\Local;

use Psr\Log\NullLogger;
use Symfony\Component\Stopwatch\Stopwatch;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures\Dummy;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\MongoClientTestSuite;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;
use Wizbii\OpenSource\MongoBundle\Storage\Local\InMemory\InMemoryDataProvider;
use Wizbii\OpenSource\MongoBundle\Storage\Local\MongoClient;

class InMemoryMongoClientTest extends MongoClientTestSuite
{
    private const NOT_IMPLEMENTED_METHODS = ['aggregate', 'aggregateAs'];

    private MongoClient $mongoClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->mongoClient = new MongoClient(
            self::DATABASE,
            self::COLLECTION,
            Dummy::class,
            new NullLogger(),
            new Stopwatch(),
            new Serializer(),
            new InMemoryDataProvider()
        );
    }

    protected function getClient(): MongoClientInterface
    {
        return $this->mongoClient;
    }

    protected function isImplemented(string $method): bool
    {
        return !in_array($method, self::NOT_IMPLEMENTED_METHODS);
    }
}
