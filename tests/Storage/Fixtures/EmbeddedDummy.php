<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class EmbeddedDummy implements ArraySerializable
{
    private string $id;
    private string $name;

    public function __construct(string $id, string $name = 'hello')
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function serialize(): array
    {
        return ['_id' => $this->id, 'name' => $this->name];
    }

    public static function deserialize(array $contentAsArray): ArraySerializable
    {
        return new self($contentAsArray['_id'], $contentAsArray['name']);
    }
}
