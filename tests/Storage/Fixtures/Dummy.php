<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class Dummy implements ArraySerializable
{
    public string $id;
    public string $foo;
    public EmbeddedDummy $embedded;
    /** @var EmbeddedDummy[] */
    public array $embeddedList;

    public static function filled(): Dummy
    {
        $instance = new self();

        $instance->id = 'dummy';
        $instance->foo = 'bar';
        $instance->embedded = (new EmbeddedDummy(1, 'foo'));
        $instance->embeddedList = [new EmbeddedDummy(2, 'hello'), new EmbeddedDummy(3, 'world')];

        return $instance;
    }

    public static function withId(string $id): Dummy
    {
        $instance = self::filled();
        $instance->id = $id;

        return $instance;
    }

    public function setFoo(string $value): Dummy
    {
        $this->foo = $value;

        return $this;
    }

    public function serialize(): array
    {
        return [
            '_id' => $this->id,
            'foo' => $this->foo,
            'embedded' => $this->embedded->serialize(),
            'embeddedList' => array_map(fn (EmbeddedDummy $embeddedDummy) => $embeddedDummy->serialize(), $this->embeddedList),
        ];
    }

    public static function deserialize(array $contentAsArray): ArraySerializable
    {
        $instance = new self();

        $instance->id = $contentAsArray['_id'];
        $instance->foo = $contentAsArray['foo'];
        $instance->embedded = EmbeddedDummy::deserialize($contentAsArray['embedded']);
        $instance->embeddedList = array_map(fn ($conf) => EmbeddedDummy::deserialize($conf), $contentAsArray['embeddedList']);

        return $instance;
    }
}
