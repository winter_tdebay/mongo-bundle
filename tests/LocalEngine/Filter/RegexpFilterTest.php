<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class RegexpFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar'];

        return [
            'valid document (short syntax)' => [$document, ['foo' => '/^ba/'], true],
            'valid document (long syntax with $regexp)' => [$document, ['foo' => ['$regexp' => '/^bar/']], true],
            'invalid document - bad value' => [$document, ['foo' => '/^ab/'], false],
            'invalid document - missing key' => [$document, ['foo2' => '/^ba/'], false],
        ];
    }
}
