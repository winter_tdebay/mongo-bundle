<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class ExistsFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar'];

        return [
            'valid document (with existing key)' => [$document, ['foo' => ['$exists' => true]], true],
            'valid document (with missing key)' => [$document, ['foo2' => ['$exists' => false]], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$exists' => false]], false],
            'invalid document (with missing key)' => [$document, ['fo2o' => ['$exists' => true]], false],
        ];
    }
}
