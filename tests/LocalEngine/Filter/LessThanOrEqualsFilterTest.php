<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class LessThanOrEqualsFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 3];

        return [
            'valid document' => [$document, ['foo' => ['$lte' => 3]], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$lte' => 2]], false],
            'invalid document (with missing key)' => [$document, ['foo2' => ['$lte' => 2]], false],
        ];
    }
}
