<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class OrFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar', 'foo2' => 'bar2', 'foo3' => 'bar3'];

        return [
            'valid document' => [$document, ['$or' => ['foo' => 'bar', 'foo4' => 'bar4']], true],
            'unknown value' => [$document, ['$or' => ['foo' => 'bar2', 'foo2' => 'bar3']], false],
            'unknown key' => [$document, ['$or' => ['foo4' => 'bar', 'fo52' => 'bar2']], false],
        ];
    }
}
