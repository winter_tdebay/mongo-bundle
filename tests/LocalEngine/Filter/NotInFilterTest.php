<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NotInFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $arrayDocument = ['foo' => ['bar', 'bar2']];
        $stringDocument = ['foo' => 'bar'];

        return [
            'valid array document (with existing key)' => [$arrayDocument, ['foo' => ['$nin' => ['bar3', 'bar4']]], true],
            'valid array document (with missing key)' => [$arrayDocument, ['foo2' => ['$nin' => ['bar']]], true],
            'invalid array document (with existing key)' => [$arrayDocument, ['foo' => ['$nin' => ['bar']]], false],
            'valid string document (with existing key)' => [$stringDocument, ['foo' => ['$nin' => ['bar3', 'bar4']]], true],
            'valid string document (with missing key)' => [$stringDocument, ['foo2' => ['$nin' => ['bar']]], true],
            'invalid string document (with existing key)' => [$stringDocument, ['foo' => ['$nin' => ['bar']]], false],
        ];
    }
}
