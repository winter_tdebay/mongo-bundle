<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class LessThanFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 3];

        return [
            'valid document' => [$document, ['foo' => ['$lt' => 4]], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$lt' => 3]], false],
            'invalid document (with missing key)' => [$document, ['foo2' => ['$lt' => 2]], false],
        ];
    }
}
