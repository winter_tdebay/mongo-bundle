<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class GreaterThanFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 3];

        return [
            'valid document' => [$document, ['foo' => ['$gt' => 2]], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$gt' => 3]], false],
            'invalid document (with missing key)' => [$document, ['foo2' => ['$gt' => 2]], false],
        ];
    }
}
