<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class AllFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => ['bar', 'bar2']];

        return [
            'valid document' => [$document, ['foo' => ['$all' => ['bar', 'bar2']]], true],
            'unknown value' => [$document, ['foo' => ['$all' => ['bar', 'bar3']]], false],
            'missing key' => [['foo2' => ['bar', 'bar2']], ['foo' => ['$all' => ['bar', 'bar2']]], false],
        ];
    }
}
