<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\AndFilter;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\FilterFactory;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\NotSupportedFilter;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\OrFilter;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\Selector;

class FilterFactoryTest extends TestCase
{
    public function testItCanBuildSimpleQuery()
    {
        $query = ['foo' => 'bar'];
        $filterFactory = new FilterFactory();
        $filter = $filterFactory->buildFilter($query);
        $this->assertThat($filter, $this->isInstanceOf(Selector::class));
    }

    public function testItCanBuildImplicitAndQuery()
    {
        $query = ['foo' => 'bar', 'foo2' => 'bar2'];
        $filterFactory = new FilterFactory();
        $filter = $filterFactory->buildFilter($query);
        $this->assertThat($filter, $this->isInstanceOf(AndFilter::class));
    }

    public function testItCanBuildCompoundQuery()
    {
        $query = ['$or' => [['foo' => 'bar'], ['foo2' => 'bar2']]];
        $filterFactory = new FilterFactory();
        $filter = $filterFactory->buildFilter($query);
        $this->assertThat($filter, $this->isInstanceOf(OrFilter::class));
    }

    public function testItCanBuildQueryWithNotSupportedFilter()
    {
        $query = ['$where' => 'function() {return (hex_md5(this.name) === "9b53e667f30cd329dca1ec9e6a83e994")}'];
        $filterFactory = new FilterFactory();
        $filter = $filterFactory->buildFilter($query);
        $this->assertThat($filter, $this->isInstanceOf(NotSupportedFilter::class));
    }
}
