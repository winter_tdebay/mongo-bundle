<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class CommentFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar', 'foo2' => 'bar2', 'foo3' => 'bar3'];

        return [
            'always true' => [$document, ['$comment' => 'this is a comment on a query. Cool for logs :)'], true],
        ];
    }
}
