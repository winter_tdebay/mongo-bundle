<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class TrueFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar'];

        return [
            'valid document (always are :) )' => [$document, [], true],
        ];
    }
}
