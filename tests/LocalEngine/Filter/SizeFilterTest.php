<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class SizeFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => ['bar', 'bar2']];

        return [
            'valid document' => [$document, ['foo' => ['$size' => 2]], true],
            'invalid document - bad value' => [$document, ['foo' => ['$size' => 3]], false],
            'invalid document - missing key' => [$document, ['foo2' => ['$size' => 2]], false],
        ];
    }
}
