<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Updater\UpdaterFactory;

abstract class UpdaterTestCase extends TestCase
{
    /**
     * @dataProvider getUseCases
     */
    public function testUpdater(array $actualDocument, array $query, array $expectedDocument, bool $debug = false)
    {
        $updater = (new UpdaterFactory())->buildUpdater($query);
        if ($debug) {
            echo $updater->debug();
        }
        $this->assertThat($updater->execute($actualDocument), $this->equalTo($expectedDocument));
    }

    abstract public function getUseCases();
}
