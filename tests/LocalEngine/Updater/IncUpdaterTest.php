<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class IncUpdaterTest extends UpdaterTestCase
{
    public function getUseCases()
    {
        return [
            'simple document' => [['foo' => 2], ['$inc' => ['foo' => 1]], ['foo' => 3]],
            'embedded document' => [['foo' => ['bar' => 2]], ['$inc' => ['foo.bar' => 1]], ['foo' => ['bar' => 3]]],
            'negative value' => [['foo' => ['bar' => 2]], ['$inc' => ['foo.bar' => -3]], ['foo' => ['bar' => -1]]],
            'multiple document' => [['foo' => 1, 'foo2' => 2], ['$inc' => ['foo' => 1, 'foo2' => 2]], ['foo' => 2, 'foo2' => 4]],
            'create missing fields' => [['foo' => ['bar' => 1]], ['$inc' => ['foo.bar' => 1, 'foo.bar2' => -1]], ['foo' => ['bar' => 2, 'bar2' => -1]]],
        ];
    }
}
