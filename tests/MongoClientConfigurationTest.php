<?php

namespace Tests\Wizbii\OpenSource\MongoBundle;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;

class MongoClientConfigurationTest extends TestCase
{
    public function testItBuildConnectionStringFromSeparatedConfigValues()
    {
        $config = new MongoClientConfiguration();

        $config->setProtocol('mongodb');
        $config->setHost('localhost');
        $config->setPort(27017);
        $config->setReplicaSet('cluster1');
        $config->setSocketTimeoutMS(30);
        $config->setRetryWrites(true);

        $this->assertEquals('mongodb://localhost:27017/?replicaSet=cluster1&wTimeoutMS=30&retryWrites=1', $config->buildConnectionString());
    }

    public function testItAcceptsAFullConnectionString()
    {
        $config = new MongoClientConfiguration();

        $config->setProtocol('mongodb');
        $config->setHost('localhost');
        $config->setPort(27017);
        $config->setReplicaSet('cluster1');
        $config->setSocketTimeoutMS(30);
        $config->setRetryWrites(false);

        $connectionString = 'mongodb://10.4.1.1:27018,10.4.1.2:27017?replicaSet=prod&wTimeoutMS=10&retryWrites=0&roles=front';

        $config->setConnectionString($connectionString);

        $this->assertEquals($connectionString, $config->buildConnectionString());
    }

    public function testItCanBuildOptions()
    {
        $config = new MongoClientConfiguration();
        $config
            ->setWriteConcern('majority')
            ->setWriteTimeoutMS(100)
            ->setReadConcernLevel('available')
            ->setReadPreference('nearest')
            ->setReadPreferenceTags('datacenter=europe-1,subset=frontend')
        ;
        $options = $config->buildOptions();
        $this->assertThat($options, $this->countOf(3));
        $this->assertThat($options, $this->arrayHasKey('w'));
        $this->assertThat($options, $this->arrayHasKey('readConcernLevel'));
        $this->assertThat($options, $this->arrayHasKey('readPreference'));
    }
}
