<?php

namespace Wizbii\OpenSource\MongoBundle;

use Wizbii\JsonSerializerBundle\ArraySerializable;

interface MongoClientBuilderInterface
{
    /**
     * @phpstan-template Y of ArraySerializable
     *
     * @phpstan-param class-string<Y> $managedClassName
     *
     * @phpstan-return MongoClientInterface<Y>
     */
    public function buildFor(string $database, string $collection, string $managedClassName): MongoClientInterface;

    /**
     * @phpstan-param class-string<ArraySerializable> $managedClassName
     */
    public function overrideConfigurationFor(string $database, string $collection, string $managedClassName): MongoClientConfiguration;

    // @phpstan-ignore-next-line Generics not defined in MongoClientConfiguration
    public function build(): MongoClientInterface;
}
