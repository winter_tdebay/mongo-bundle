<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class IncUpdater implements Updater
{
    private array $incrementers;

    public function __construct(array $incrementers)
    {
        $this->incrementers = $incrementers;
    }

    public function execute(array $document): array
    {
        foreach ($this->incrementers as $path => $increment) {
            $parts = explode('.', $path);
            $currentSubDocument = &$document;
            $part = null;
            while (count($parts) > 1) {
                /** @var string $part */
                $part = array_shift($parts);
                if (!array_key_exists($part, $currentSubDocument)) {
                    $currentSubDocument[$part] = [];
                }
                $currentSubDocument = &$currentSubDocument[$part];
            }
            if (!array_key_exists($parts[0], $currentSubDocument)) {
                $currentSubDocument[$parts[0]] = 0;
            }
            $currentSubDocument[$parts[0]] = $currentSubDocument[$parts[0]] + $increment;
        }

        return $document;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        $content = str_repeat(' ', $nbSpaces)."IncUpdater: \n";
        foreach ($this->incrementers as $path => $increment) {
            $content .= str_repeat(' ', $nbSpaces + 2).$path.' => '.json_encode($increment)."\n";
        }

        return $content;
    }
}
