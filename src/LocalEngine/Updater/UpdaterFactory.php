<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class UpdaterFactory
{
    /** @see https://docs.mongodb.com/manual/reference/operator/update/#id1 */
    private const SUPPORTED_UPDATERS = [
        // Fields
        '$currentDate' => ['class' => NotSupportedUpdater::class],
        '$inc' => ['class' => IncUpdater::class],
        '$min' => ['class' => NotSupportedUpdater::class],
        '$max' => ['class' => NotSupportedUpdater::class],
        '$mul' => ['class' => NotSupportedUpdater::class],
        '$rename' => ['class' => NotSupportedUpdater::class],
        '$set' => ['class' => SetUpdater::class],
        '$setOnInsert' => ['class' => NotSupportedUpdater::class],
        '$unset' => ['class' => NotSupportedUpdater::class],

        // Array
        '$addToSet' => ['class' => NotSupportedUpdater::class],
        '$pop' => ['class' => NotSupportedUpdater::class],
        '$pull' => ['class' => NotSupportedUpdater::class],
        '$push' => ['class' => NotSupportedUpdater::class],
        '$pushAll' => ['class' => NotSupportedUpdater::class],

        // Modifiers
        '$each' => ['class' => NotSupportedUpdater::class],
        '$position' => ['class' => NotSupportedUpdater::class],
        '$slice' => ['class' => NotSupportedUpdater::class],
        '$sort' => ['class' => NotSupportedUpdater::class],

        // Bitwise
        '$bit' => ['class' => NotSupportedUpdater::class],
    ];

    public function buildUpdater(array $query): Updater
    {
        foreach ($query as $key => $value) {
            if (array_key_exists($key, self::SUPPORTED_UPDATERS)) {
                $class = self::SUPPORTED_UPDATERS[$key]['class'];
                if ($class === NotSupportedUpdater::class) {
                    return new $class($key);
                }

                return new $class($value);
            }
        }

        return new IdentityUpdater();
    }
}
