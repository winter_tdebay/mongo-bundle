<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

use Wizbii\OpenSource\MongoBundle\LocalEngine\Exception\UpdaterNotSupportedException;

class NotSupportedUpdater implements Updater
{
    private string $updater;

    public function __construct(string $updater)
    {
        $this->updater = $updater;
    }

    public function execute(array $document): array
    {
        throw new UpdaterNotSupportedException($this->updater);
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."NotSupportedUpdater: '".$this->updater."'";
    }
}
