<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class SetUpdater implements Updater
{
    private array $setters;

    public function __construct(array $setters)
    {
        $this->setters = $setters;
    }

    public function execute(array $document): array
    {
        foreach ($this->setters as $path => $newValue) {
            $parts = explode('.', $path);
            $currentSubDocument = &$document;
            $part = null;
            while (count($parts) > 1) {
                /** @var string $part */
                $part = array_shift($parts);
                if (!array_key_exists($part, $currentSubDocument)) {
                    $currentSubDocument[$part] = [];
                }
                $currentSubDocument = &$currentSubDocument[$part];
            }
            $currentSubDocument[$parts[0]] = $newValue;
        }

        return $document;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        $content = str_repeat(' ', $nbSpaces)."SetUpdater: \n";
        foreach ($this->setters as $path => $newValue) {
            $content .= str_repeat(' ', $nbSpaces + 2).$path.' => '.json_encode($newValue)."\n";
        }

        return $content;
    }
}
