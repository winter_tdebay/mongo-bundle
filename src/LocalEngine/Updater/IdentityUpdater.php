<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class IdentityUpdater implements Updater
{
    public function execute(array $document): array
    {
        return $document;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."IdentityUpdater: \n";
    }
}
