<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class FilterFactory
{
    /** @see https://docs.mongodb.com/manual/reference/operator/query/ */
    private const SUPPORTED_FILTERS = [
        // Comparison
        '$eq' => ['class' => EqualsFilter::class, 'options' => ['single-scalar-as-children']],
        '$gt' => ['class' => GreaterThanFilter::class, 'options' => ['single-scalar-as-children']],
        '$gte' => ['class' => GreaterThanOrEqualsFilter::class, 'options' => ['single-scalar-as-children']],
        '$in' => ['class' => InFilter::class, 'options' => ['multiple-scalars-as-children']],
        '$lt' => ['class' => LessThanFilter::class, 'options' => ['single-scalar-as-children']],
        '$lte' => ['class' => LessThanOrEqualsFilter::class, 'options' => ['single-scalar-as-children']],
        '$ne' => ['class' => NotEqualsFilter::class, 'options' => ['single-scalar-as-children']],
        '$nin' => ['class' => NotInFilter::class, 'options' => ['multiple-scalars-as-children']],

        // Logical
        '$and' => ['class' => AndFilter::class, 'options' => ['multiple-filters-as-children']],
        '$not' => ['class' => NotFilter::class],
        '$nor' => ['class' => NorFilter::class, 'options' => ['multiple-filters-as-children']],
        '$or' => ['class' => OrFilter::class, 'options' => ['multiple-filters-as-children']],

        // Element
        '$exists' => ['class' => ExistsFilter::class, 'options' => ['single-scalar-as-children']],
        '$type' => ['class' => TypeFilter::class, 'options' => ['single-scalar-as-children']],

        // Evaluation
        '$expr' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$jsonSchema' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$mod' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$regexp' => ['class' => RegexpFilter::class, 'options' => ['single-scalar-as-children']],
        '$text' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$where' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],

        // Geospatial
        '$geoIntersects' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$geoWithin' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$near' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$nearSphere' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],

        // Array
        '$all' => ['class' => AllFilter::class, 'options' => ['multiple-scalars-as-children']],
        '$elemMatch' => ['class' => ElemMatchFilter::class, 'options' => ['multiple-filters-as-children']],
        '$size' => ['class' => SizeFilter::class, 'options' => ['single-scalar-as-children']],

        // Bitwise
        '$bitsAllClear' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$bitsAllSet' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$bitsAnyClear' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],
        '$bitsAnySet' => ['class' => NotSupportedFilter::class, 'options' => ['require-filter-name']],

        // Comment
        '$comment' => ['class' => CommentFilter::class, 'options' => ['single-scalar-as-children']],
    ];

    public function buildFilter($query): Filter
    {
        if (is_null($query)) {
            return new EqualsFilter(null);
        }
        if (is_array($query) && empty($query)) {
            return new TrueFilter();
        }
        if (is_scalar($query)) {
            return is_string($query) && $this->startsWith($query, '/') ? new RegexpFilter($query) : new EqualsFilter($query);
        }
        /** @var array $query */
        $filters = $this->getMultipleFilters($query);

        return count($filters) > 1 ? new AndFilter($filters) : $filters[0];
    }

    private function getMultipleFilters(array $query): array
    {
        $filters = [];
        if ($this->isAssociativeArray($query)) {
            foreach ($query as $key => $value) {
                $filters[] = $this->getSingleFilter($key, $value);
            }
        } else {
            foreach ($query as $value) {
                $filters[] = $this->buildFilter($value);
            }
        }

        return $filters;
    }

    /**
     * @phpsta
     */
    private function getSingleFilter(string $key, $value): Filter
    {
        if (array_key_exists($key, self::SUPPORTED_FILTERS)) {
            $class = self::SUPPORTED_FILTERS[$key]['class'];
            if (in_array('multiple-filters-as-children', self::SUPPORTED_FILTERS[$key]['options'] ?? [])) {
                return new $class($this->getMultipleFilters($value));
            } elseif (in_array('multiple-scalars-as-children', self::SUPPORTED_FILTERS[$key]['options'] ?? [])) {
                return new $class($value);
            } elseif (in_array('single-scalar-as-children', self::SUPPORTED_FILTERS[$key]['options'] ?? [])) {
                return new $class($value);
            } elseif (in_array('require-filter-name', self::SUPPORTED_FILTERS[$key]['options'] ?? [])) {
                return new $class($key);
            }

            return new $class($this->buildFilter($value));
        }

        return new Selector($key, $this->buildFilter($value));
    }

    private function isAssociativeArray(array $a): bool
    {
        return is_array($a) && array_keys($a) !== range(0, count($a) - 1);
    }

    private function startsWith(string $haystack, string $needle): bool
    {
        // search backwards starting from haystack length characters from the end
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
}
