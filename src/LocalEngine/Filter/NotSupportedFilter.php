<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use Wizbii\OpenSource\MongoBundle\LocalEngine\Exception\FilterNotSupportedException;

class NotSupportedFilter implements Filter
{
    private string $filter;

    public function __construct(string $filter)
    {
        $this->filter = $filter;
    }

    public function matches(mixed $value): bool
    {
        throw new FilterNotSupportedException($this->filter);
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."NotSupportedFilter: '".$this->filter."'";
    }
}
