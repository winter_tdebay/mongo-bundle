<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use Wizbii\OpenSource\MongoBundle\LocalEngine\Exception\TypeNotSupportedException;

class TypeFilter implements Filter
{
    private const SUPPORTED_TYPES = [
        'double' => 'is_double',
        1 => 'is_double',
        'string' => 'is_string',
        2 => 'is_string',
        'object' => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_real_object',
        3 => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_real_object',
        'array' => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_real_array',
        4 => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_real_array',
        'binData' => 'is_string', // cannot do much more in PHP
        5 => 'is_string',
        'objectId' => 'is_string',
        7 => 'is_string',
        'bool' => 'is_bool',
        8 => 'is_bool',
        'is_date' => 'is_string', // can be improved
        9 => 'is_string',
        'null' => 'is_null',
        10 => 'is_null',
        'regex' => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_regexp',
        11 => '\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\is_regexp',
        'javascript' => 'is_string',
        13 => 'is_string',
        'javascriptWithScope' => 'is_string', // can be improved
        15 => 'is_string',
        'int' => 'is_int',
        16 => 'is_int',
        'timestamp' => 'is_int',
        17 => 'is_int',
        'long' => 'is_long',
        18 => 'is_long',
        'decimal' => 'is_float',
        19 => 'is_float',
        'minKey' => 'is_int', // can be improved
        -1 => 'is_int',
        'maxKey' => 'is_int', // can be improved
        127 => 'is_int',
        'number' => 'is_numeric',
    ];

    private array $types;

    public function __construct(mixed $types)
    {
        $this->types = is_array($types) ? $types : [$types];
    }

    public function matches(mixed $value): bool
    {
        foreach ($this->types as $type) {
            if (!array_key_exists($type, self::SUPPORTED_TYPES)) {
                throw new TypeNotSupportedException($type);
            }
            /** @var callable $function */
            $function = self::SUPPORTED_TYPES[$type];
            if ($function($value)) {
                return true;
            }
        }

        return false;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."TypeFilter: '".var_export($this->types, true)."'";
    }
}

function is_real_object(mixed $a): bool
{
    return is_array($a) && array_keys($a) !== range(0, count($a) - 1);
}

function is_real_array(mixed $a): bool
{
    return is_array($a) && array_keys($a) === range(0, count($a) - 1);
}

function is_regexp(mixed $a): bool
{
    return is_string($a) && strrpos($a, '/', -strlen($a)) !== false;
}
