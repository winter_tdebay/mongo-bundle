<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class Selector implements Filter
{
    private string $fieldId;
    private Filter $filter;

    public function __construct(string $fieldId, Filter $filter)
    {
        $this->fieldId = $fieldId;
        $this->filter = $filter;
    }

    /**
     * @internal
     *
     * @return mixed|null
     */
    public function select(string $fieldId, array $document)
    {
        $parts = explode('.', $fieldId);
        /** @var string $firstPart */
        $firstPart = array_shift($parts);
        if (!array_key_exists($firstPart, $document)) {
            return null;
        }
        $subDocument = $document[$firstPart];
        if (empty($parts)) {
            return $subDocument;
        } else {
            return $this->select(join('.', $parts), $subDocument);
        }
    }

    public function matches(mixed $value): bool
    {
        /** @var array $value */
        $value2 = $this->select($this->fieldId, $value);

        return $this->filter->matches($value2);
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        $content = str_repeat(' ', $nbSpaces)."Selector:\n";
        $content .= str_repeat(' ', $nbSpaces + 2).'fieldId: '.$this->fieldId."\n";
        $content .= str_repeat(' ', $nbSpaces + 2)."filter:\n".$this->filter->debug($nbSpaces + 4);

        return $content;
    }
}
