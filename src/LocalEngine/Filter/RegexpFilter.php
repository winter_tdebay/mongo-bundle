<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class RegexpFilter implements Filter
{
    private string $regexp;

    public function __construct(string $regexp)
    {
        $this->regexp = $regexp;
    }

    public function matches(mixed $value): bool
    {
        return is_string($value) && preg_match($this->regexp, $value) === 1;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."RegexpFilter: '".$this->regexp."'";
    }
}
