<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NotEqualsFilter implements Filter
{
    private mixed $value;

    public function __construct(mixed $value)
    {
        $this->value = $value;
    }

    public function matches(mixed $value): bool
    {
        return $this->value !== $value;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."NotEqualsFilter: '".$this->value."'";
    }
}
