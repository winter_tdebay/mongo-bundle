<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NotInFilter implements Filter
{
    private array $values;

    public function __construct(array $values)
    {
        $this->values = $values;
    }

    public function matches(mixed $value): bool
    {
        if (!is_array($value) && !empty($value)) {
            $value = [$value];
        }

        if (empty($value)) {
            return true;
        }

        if (is_array($value)) {
            foreach ($this->values as $v) {
                if (in_array($v, $value)) {
                    return false;
                }
            }
        }

        return true;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."NotInFilter: '".var_export($this->values, true)."'";
    }
}
