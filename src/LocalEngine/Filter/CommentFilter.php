<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class CommentFilter implements Filter
{
    private string $comment;

    public function __construct(string $comment)
    {
        $this->comment = $comment;
    }

    public function matches(mixed $value): bool
    {
        return true;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."CommentFilter: '".$this->comment."'";
    }
}
