<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class SizeFilter implements Filter
{
    private int $size;

    public function __construct(int $size)
    {
        $this->size = $size;
    }

    public function matches(mixed $value): bool
    {
        return is_array($value) && count($value) === $this->size;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."SizeFilter: '".$this->size."'";
    }
}
