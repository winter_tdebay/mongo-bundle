<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class ExistsFilter implements Filter
{
    private bool $mustExists;

    public function __construct(bool $mustExists)
    {
        $this->mustExists = $mustExists;
    }

    public function matches(mixed $value): bool
    {
        return ($this->mustExists && $value !== null) || (!$this->mustExists && $value === null);
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."ExistsFilter: '".$this->mustExists."'";
    }
}
