<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Exception;

class UpdaterNotSupportedException extends \Exception
{
    public function __construct(string $updater, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct("Updater '$updater' is not supported by this library. Open a MR to support it", $code, $previous);
    }
}
