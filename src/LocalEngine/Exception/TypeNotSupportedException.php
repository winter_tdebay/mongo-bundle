<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Exception;

class TypeNotSupportedException extends \Exception
{
    public function __construct(string $type = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct("Type '$type' is not supported by mongodb for the \$type filter", $code, $previous);
    }
}
