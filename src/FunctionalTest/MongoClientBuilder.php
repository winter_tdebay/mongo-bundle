<?php

namespace Wizbii\OpenSource\MongoBundle\FunctionalTest;

use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\Storage\Mongo\MongoClientBuilder as MongoClientBuilderForMongo;

class MongoClientBuilder extends MongoClientBuilderForMongo
{
    public function overrideConfigurationFor(string $database, string $collection, string $managedClassName): MongoClientConfiguration
    {
        return parent::overrideConfigurationFor("test_$database", $collection, $managedClassName);
    }
}
