<?php

namespace Wizbii\OpenSource\MongoBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/** @codeCoverageIgnore */
class WizbiiOpenSourceMongoBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
    }
}
