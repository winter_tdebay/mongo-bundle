<?php

namespace Wizbii\OpenSource\MongoBundle\Storage;

use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\JsonSerializerBundle\Exception\DeserializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\Exception\DocumentDoesNotExistException;
use Wizbii\OpenSource\MongoBundle\Exception\MethodNotImplementedInThisStorageException;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;

/**
 * @phpstan-template T of ArraySerializable
 *
 * @phpstan-implements MongoClientInterface<T>
 */
abstract class AbstractMongoClient implements MongoClientInterface
{
    protected string $collectionName;
    protected string $databaseName;
    /** @phpstan-var class-string<T> */
    protected string $managedClassName;
    protected Serializer $serializer;
    protected LoggerInterface $logger;
    protected Stopwatch $stopwatch;

    /**
     * @phpstan-param class-string<T> $managedClassName
     */
    public function __construct(string $databaseName, string $collectionName, string $managedClassName, LoggerInterface $logger, Stopwatch $stopwatch, Serializer $serializer)
    {
        $this->databaseName = $databaseName;
        $this->collectionName = $collectionName;
        $this->managedClassName = $managedClassName;
        $this->logger = $logger;
        $this->stopwatch = $stopwatch;
        $this->serializer = $serializer;
    }

    public function aggregate(array $pipeline, bool $outputAsArray = true): \Generator
    {
        throw new MethodNotImplementedInThisStorageException(__CLASS__, __METHOD__);
    }

    public function aggregateAs(array $pipeline, string $className): \Generator
    {
        throw new MethodNotImplementedInThisStorageException(__CLASS__, __METHOD__);
    }

    /** @phpstan-return T[] */
    public function find(?int $rows = 10, int $offset = 0, ?array $sort = null): array
    {
        return $this->findBy([], $rows, $offset, $sort);
    }

    /** @phpstan-return T */
    public function findOne(array $query): ?ArraySerializable
    {
        $documents = $this->findBy($query, 1);

        return empty($documents) ? null : $documents[0];
    }

    /** @phpstan-return T */
    public function get(string $id): ArraySerializable
    {
        $document = $this->findOne(['_id' => $id]);
        if (!isset($document)) {
            throw new DocumentDoesNotExistException($this->getDatabaseName(), $this->getCollectionName(), $id);
        }

        return $document;
    }

    public function getCollectionName(): string
    {
        return $this->collectionName;
    }

    public function getDatabaseName(): string
    {
        return $this->databaseName;
    }

    public function has(string $id): bool
    {
        return $this->count(['_id' => $id]) === 1;
    }

    /** @phpstan-param T[] $documents */
    public function putAll(array $documents): bool
    {
        $result = true;
        foreach ($documents as $document) {
            $result = $this->put($document) && $result;
        }

        return $result;
    }

    public function remove(string $id): int
    {
        return $this->removeBy(['_id' => $id]);
    }

    /*************
     * Utilities *
     *************/
    protected function buildOptions(?int $rows = null, ?int $offset = null, ?array $sort = []): array
    {
        $options = [];
        if (isset($rows)) {
            $options['limit'] = $rows;
        }
        if (isset($offset)) {
            $options['skip'] = $offset;
        }
        if (isset($sort)) {
            $options['sort'] = $sort;
        }

        return $options;
    }

    /** @phpstan-return T[] */
    protected function readDocuments(array $documents): array
    {
        $docs = [];
        foreach ($documents as $document) {
            $docs[] = $this->readDocument($document);
        }

        return $docs;
    }

    /** @phpstan-return T */
    protected function readDocument(array $document): ArraySerializable
    {
        return $this->readDocumentAs($document, $this->managedClassName);
    }

    /**
     * @phpstan-template Y of ArraySerializable
     *
     * @phpstan-param class-string<Y> $className
     *
     * @phpstan-return Y
     */
    protected function readDocumentAs(array $document, string $className): ArraySerializable
    {
        $this->stopwatch->start('mongo.read');
        try {
            /** @var string $documentAsString */
            $documentAsString = json_encode($document);
            /** @phpstan-var Y $content */
            $content = $this->serializer->deserialize($documentAsString, $className);
            $this->stopwatch->stop('mongo.read');

            return $content;
        } catch (DeserializationNotSupportedException $e) {
            $this->logger->error("Can't read document. Error was : ".$e->getMessage(), ['exception' => $e]);
            $this->stopwatch->stop('mongo.read');
            throw $e;
        }
    }
}
