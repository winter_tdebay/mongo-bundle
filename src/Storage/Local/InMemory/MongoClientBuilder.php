<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local\InMemory;

use Psr\Log\NullLogger;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;
use Wizbii\OpenSource\MongoBundle\Storage\Local\LocalMongoClientBuilder;
use Wizbii\OpenSource\MongoBundle\Storage\Local\MongoClient;

class MongoClientBuilder extends LocalMongoClientBuilder
{
    public function overrideConfigurationFor(string $database, string $collection, string $managedClassName): MongoClientConfiguration
    {
        $mongoClientConfigurationCloned = (clone $this->mongoClientConfiguration)
            ->setDatabase($database)
            ->setCollection($collection)
            ->setManagedClassName($managedClassName)
        ;

        $mongoClientBuilder = new self($mongoClientConfigurationCloned);
        $mongoClientConfigurationCloned->setMongoClientBuilder($mongoClientBuilder);

        return $mongoClientConfigurationCloned;
    }

    protected function doBuild(): MongoClientInterface
    {
        return new MongoClient(
            $this->mongoClientConfiguration->getDatabase(),
            $this->mongoClientConfiguration->getCollection(),
            $this->mongoClientConfiguration->getManagedClassName(),
            new NullLogger(),
            new Stopwatch(),
            new Serializer(),
            new InMemoryDataProvider()
        );
    }
}
