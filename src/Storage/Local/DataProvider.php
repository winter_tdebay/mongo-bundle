<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local;

interface DataProvider
{
    public function getAll(): array;

    public function saveAll(array $documents): void;

    public function removeAll(): bool;
}
