<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local\File;

use Wizbii\OpenSource\MongoBundle\Storage\Local\DataProvider;

class FileDataProvider implements DataProvider
{
    private string $directory;
    private string $filename;

    public function __construct(string $directory, string $filename)
    {
        $this->directory = $directory;
        $this->filename = $filename;
    }

    public function getAll(): array
    {
        if (!is_readable($this->getFilepath())) {
            return [];
        }

        /** @var string $content */
        $content = file_get_contents($this->getFilepath());
        /** @var array $array */
        $array = json_decode($content, true);

        return $array;
    }

    public function saveAll(array $documents): void
    {
        if (!is_dir($this->directory)) {
            mkdir($this->directory, 0o755, true);
        }
        file_put_contents($this->getFilepath(), json_encode($documents));
    }

    public function dropDirectory(): void
    {
        $this->recursiveDropDirectory($this->directory);
    }

    public function removeAll(): bool
    {
        $this->dropDirectory();

        return true;
    }

    private function getFilepath(): string
    {
        return $this->directory.'/'.$this->filename;
    }

    private function recursiveDropDirectory(string $directory): bool
    {
        if (!file_exists($directory)) {
            return true;
        }

        if (!is_dir($directory)) {
            return unlink($directory);
        }
        /** @var array $items */
        $items = scandir($directory);
        foreach ($items as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->recursiveDropDirectory($directory.DIRECTORY_SEPARATOR.$item)) {
                return false;
            }
        }

        return rmdir($directory);
    }
}
