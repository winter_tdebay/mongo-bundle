<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local;

use MongoDB\Operation\FindOneAndUpdate;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\FilterFactory;
use Wizbii\OpenSource\MongoBundle\LocalEngine\SortExecutor;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Updater\UpdaterFactory;
use Wizbii\OpenSource\MongoBundle\Storage\AbstractMongoClient;

/**
 * @phpstan-template T of ArraySerializable
 *
 * @phpstan-extends AbstractMongoClient<T>
 */
class MongoClient extends AbstractMongoClient
{
    private DataProvider $dataProvider;

    /**
     * @phpstan-param class-string<T> $managedClassName
     */
    public function __construct(string $databaseName, string $collectionName, string $managedClassName, LoggerInterface $logger, Stopwatch $stopwatch, Serializer $serializer, DataProvider $dataProvider)
    {
        parent::__construct($databaseName, $collectionName, $managedClassName, $logger, $stopwatch, $serializer);
        $this->dataProvider = $dataProvider;
    }

    public function count(array $query = [], int $retriesCount = 0): int
    {
        return count($this->internalFind($this->dataProvider->getAll(), $query));
    }

    /** @phpstan-return \Generator<T> */
    public function each(array $query, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator
    {
        $options = $this->buildOptions($rows, $offset, $sort);
        $documents = $this->readDocuments($this->internalFind($this->dataProvider->getAll(), $query, $options));
        foreach ($documents as $document) {
            yield $document;
        }
    }

    public function eachWithProjection(array $query, array $fields, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator
    {
        $options = $this->buildOptions($rows, $offset);
        $documents = $this->internalFind($this->dataProvider->getAll(), $query, $options);
        foreach ($documents as $document) {
            // not really a projection but easier to implement and it should not create issues. Projections are mostly
            // used for performance issues which we are not dealing with in this context
            yield $document;
        }
    }

    /** @phpstan-return T[] */
    public function findBy(array $query = [], ?int $rows = 10, int $offset = 0, ?array $sort = null, int $retriesCount = 0): array
    {
        $options = $this->buildOptions($rows, $offset, $sort);

        return $this->readDocuments($this->internalFind($this->dataProvider->getAll(), $query, $options));
    }

    /** @phpstan-return T|null */
    public function findOneAndUpdate(array $query, array $update, array $options = []): ?ArraySerializable
    {
        $matchingDocuments = $this->internalFind($this->dataProvider->getAll(), $query, $options);
        if (empty($matchingDocuments)) {
            return null;
        }
        $document = $matchingDocuments[array_key_first($matchingDocuments)];
        $this->updateOne(['_id' => $document['_id']], $update);

        if (($options['returnDocument'] ?? FindOneAndUpdate::RETURN_DOCUMENT_BEFORE) === FindOneAndUpdate::RETURN_DOCUMENT_BEFORE) {
            return $this->readDocument($document);
        }

        return $this->get($document['_id']);
    }

    public function getDistinctValues(string $key, array $query = []): array
    {
        $documents = $this->internalFind($this->dataProvider->getAll(), $query);
        $values = array_map(fn ($document) => $this->getValueInsideDocument($document, $key), $documents);

        return array_values(array_unique($values));
    }

    /** @phpstan-param T $document */
    public function put(ArraySerializable $document, int $retriesCount = 0): bool
    {
        $documents = $this->dataProvider->getAll();
        $documentAsArray = $document->serialize();
        $index = $this->findIndex($documents, $documentAsArray['_id']);
        if ($index === -1) {
            $documents[] = $documentAsArray;
        } else {
            $documents[$index] = $documentAsArray;
        }

        $this->dataProvider->saveAll($documents);

        return true;
    }

    public function removeBy(array $query): int
    {
        $documents = $this->dataProvider->getAll();
        $matchingDocuments = $this->internalFind($documents, $query);
        if (empty($matchingDocuments)) {
            return 0;
        }
        $documents = array_filter($documents, fn ($document) => !in_array($document, $matchingDocuments));
        $this->dataProvider->saveAll($documents);

        return count($matchingDocuments);
    }

    public function update(array $query, array $update, bool $multi = true, bool $upsert = false): bool
    {
        if (!$multi) {
            return $this->updateOne($query, $update, $upsert);
        }

        $documents = $this->dataProvider->getAll();
        $matchingDocuments = $this->internalFind($documents, $query);
        $updater = (new UpdaterFactory())->buildUpdater($update);
        if (!empty($matchingDocuments)) {
            foreach ($matchingDocuments as $document) {
                $index = $this->findIndex($documents, $document['_id']);
                $documents[$index] = $updater->execute($document);
            }
        } elseif ($upsert) {
            $documents[] = $updater->execute([]);
        } else {
            return false;
        }
        $this->dataProvider->saveAll($documents);

        return true;
    }

    public function updateOne(array $query, array $update, bool $upsert = false): bool
    {
        $documents = $this->dataProvider->getAll();
        $matchingDocuments = $this->internalFind($documents, $query);
        $updater = (new UpdaterFactory())->buildUpdater($update);
        if (!empty($matchingDocuments)) {
            $document = $matchingDocuments[0];
            $index = $this->findIndex($documents, $document['_id']);
            $documents[$index] = $updater->execute($document);
        } elseif ($upsert) {
            $documents[] = $updater->execute([]);
        } else {
            return false;
        }
        $this->dataProvider->saveAll($documents);

        return true;
    }

    public function dropDatabase(): bool
    {
        $this->dataProvider->removeAll();

        return true;
    }

    public function dropCollection(): bool
    {
        $this->dataProvider->removeAll();

        return true;
    }

    public function createIndex(array $keys, array $options = []): bool
    {
        return true;
    }

    /*************
     * Utilities *
     *************/
    private function internalFind(array $documents, array $query, array $options = []): array
    {
        $filter = (new FilterFactory())->buildFilter($query);
        $matchingDocuments = [];
        foreach ($documents as $document) {
            if ($filter->matches($document)) {
                $matchingDocuments[] = $document;
            }
        }

        $matchingDocuments = (new SortExecutor())->sortDocumentsOn($matchingDocuments, $options['sort'] ?? []);
        if (($options['skip'] ?? 0) > 0) {
            $matchingDocuments = array_slice($matchingDocuments, $options['skip']);
        }
        if (($options['limit'] ?? 0) > 0) {
            $matchingDocuments = array_slice($matchingDocuments, 0, $options['limit']);
        }

        return $matchingDocuments;
    }

    /**
     * @return mixed|null
     */
    private function getValueInsideDocument(array $document, string $path)
    {
        $parts = explode('.', $path);
        /** @var string $firstPart */
        $firstPart = array_shift($parts);
        if (!array_key_exists($firstPart, $document)) {
            return null;
        } elseif (empty($parts)) {
            return $document[$firstPart];
        }

        return $this->getValueInsideDocument($document[$firstPart], join('.', $parts));
    }

    private function findIndex(array $documents, string $id): int
    {
        foreach ($documents as $index => $document) {
            if ($document['_id'] === $id) {
                return $index;
            }
        }

        return -1;
    }
}
