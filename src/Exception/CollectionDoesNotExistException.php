<?php

namespace Wizbii\OpenSource\MongoBundle\Exception;

class CollectionDoesNotExistException extends \Exception
{
    public const EXCEPTION_CODE = 1;

    public function __construct(string $databaseName, string $collectionName, ?\Throwable $previous = null)
    {
        parent::__construct("Collection with id '$collectionName' does not exist in database '$databaseName'", self::EXCEPTION_CODE, $previous);
    }
}
